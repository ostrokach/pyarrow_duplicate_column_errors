import pyarrow as pa
import pyarrow.parquet as pq


table = pq.read_table('duplicate_columns.parquet')
pq.write_table(table, '/tmp/delete.parquet')
