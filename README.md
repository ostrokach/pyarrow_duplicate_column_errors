## Segmentation faults

```bash
$ python test_segfault_1.py
Segmentation fault
```

```bash
$ python test_segfault_2.py
Segmentation fault
```

## Memory leak

```bash
$ python test_memory_leak.py
No memory leak:
Iteration 0|MaximumResidentSetSize: 170.152
Iteration 1|MaximumResidentSetSize: 174.464
Iteration 2|MaximumResidentSetSize: 174.464
Memory leak:
Iteration 0|MaximumResidentSetSize: 2828.688
Iteration 1|MaximumResidentSetSize: 4455.696
Iteration 2|MaximumResidentSetSize: 6105.976
```
