import gc
import resource

import pyarrow as pa
import pyarrow.parquet as pq


def convert_1(infile):
    # String / float / int columns do not cause a memory leak
    columns = [
        # '__index_level_0__'
        'uniparc_id', 
        'sequence', 'database', 'interpro_name', 'interpro_id',
        'domain_start', 'domain_end', 'domain_length',
        'structure_id', 'model_id', 'chain_id', 'pc_identity',
        'alignment_length', 'mismatches', 'gap_opens', 'q_start', 'q_end',
        's_start', 's_end', 'evalue_log10', 'bitscore', 'qseq', 'sseq',
    ]
    table = pq.read_table(infile, columns=columns)
    df = table.to_pandas()


def convert_2(infile):
    # Array columns cause a memory leak
    columns = [
        'a2b', 'b2a',
        'residue_idx_1', 'residue_idx_2',
        'residue_id_1', 'residue_id_2',
        'residue_aa_1', 'residue_aa_2', 
        'residue_idx_1_corrected', 'residue_idx_2_corrected',
    ]
    table = pq.read_table(infile, columns=columns)
    df = table.to_pandas()


def print_memory_usage(i):
    print((f"Iteration {i}|MaximumResidentSetSize: "
           f"{resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1000}"))


def main():
    infile = 'duplicate_columns.parquet'

    print("No memory leak:")
    for i in range(3):
        convert_1(infile)
        gc.collect()
        print_memory_usage(i)

    print("Memory leak:")
    for i in range(3):
        convert_2(infile)
        gc.collect()
        print_memory_usage(i)


if __name__ == '__main__':
    main()
